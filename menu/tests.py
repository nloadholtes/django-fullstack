from django.test import TestCase
from django.test import Client
import json

MENU_URL = "/api/v1/menu"


class ViewTests(TestCase):
    fixtures = ['menu/fixtures/nodes.json']
    def setUp(self):
        self.client = Client()

    def test_menu_get(self):
        resp = self.client.get(MENU_URL)
        self.assertTrue(resp.status_code == 200)
        menu = json.loads(resp.content)
        self.assertEqual(menu[0]["name"], "top")

    def test_menu_post_get(self):
        data = {"name": "top level", "parent": 1}
        resp = self.client.post(MENU_URL, data=json.dumps(data), content_type="application/json")
        self.assertTrue(resp.status_code == 200)
        self.assertEqual(resp.content, '{"id": 4, "parent": 1, "name": "top level"}')
        new_node = json.loads(resp.content)
        new_node["id"] = 4
        resp = self.client.get(MENU_URL)
        self.assertTrue(resp.status_code == 200)
        resp_data = json.loads(resp.content)
        self.assertTrue(new_node in resp_data)

    def test_menu_put(self):
        data = {"name": "changed", "parent": 1, "id":2}
        resp = self.client.put(MENU_URL, data=json.dumps(data), content_type="application/json")
        self.assertTrue(resp.status_code == 200)
        node = json.loads(resp.content)
        self.assertTrue(node["id"] == 2)
        self.assertTrue(node["name"] == data["name"])

    def test_menu_delete(self):
        resp = self.client.get(MENU_URL)
        nodes = json.loads(resp.content)
        self.assertTrue(len(nodes) == 3)
        resp = self.client.delete(MENU_URL, data=json.dumps({"id": 3}), content_type="application/json")
        self.assertTrue(resp.status_code == 200)
        resp = self.client.get(MENU_URL)
        nodes = json.loads(resp.content)
        self.assertTrue(len(nodes) == 2)
