import json
from django.http import HttpResponse
from django.forms.models import model_to_dict
from django.db import models


def _json_serializer(data):
    if isinstance(data, models.Model):
        return model_to_dict(data)


def json_response(data):
    """Wrapper for JSON responses"""

    return HttpResponse(json.dumps(data, default=_json_serializer), content_type="application/json")