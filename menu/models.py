from django.db import models
from mptt.models import TreeForeignKey, MPTTModel



class Node(MPTTModel):
    name = models.CharField(max_length=128)
    parent = TreeForeignKey('self',
                            null=True,
                            blank=True,
                            related_name="children",
                            db_index=True)


class Category(Node):
    pass


class Product(Node):
    price = models.FloatField()