from django.views.decorators.http import require_http_methods
from django.shortcuts import render
from django.views.generic import View
from utils import json_response
from models import Node, Product
import logging
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
import json

logger = logging.getLogger(__name__)


@require_http_methods("GET")
def mainpage(request):
    """The main page for this project"""
    return render(request, "index.html", {"menu": Node.objects.all()})


@method_decorator(csrf_exempt, name="dispatch")
class MenuView(View):
    def get(self, request):
        try:
            output = Node.objects.all()
        except Exception as e:
            logger.exception(e)
            output = None
            print(e)
        return json_response([n for n in output])

    def post(self, request):
        """Add a menu item"""
        try:
            print(request.body)
            data = json.loads(request.body)
            parent = Node.objects.filter(id=data.get("parent"))
            if parent.count() == 1:
                if hasattr(parent[0], "price"):
                    logger.debug("Tried to make a parent out of a product leaf node")
                    return json_response(None)
                data["parent"] = parent[0]
            if "price" in data:
                print("Saw a product!")
                data["price"] = float(data["price"])
                node = Product(**data)
            else:
                node = Node(**data)
            node.save()
        except Exception as e:
            logger.exception(e)
            node = "Exception"
            print(e)
        return json_response(node)

    def put(self, request):
        """Modify a menu node"""
        try:
            data = json.loads(request.body)
            node = Node.objects.get(id=data.get("id"))
            parent = Node.objects.get(id=data.get("parent"))
            if node:
                del data["id"]
                data["parent"] = parent
                for key, value in data.items():
                    node.__setattr__(key, value)
                node.save(update_fields=data.keys())
        except Exception as e:
            logger.exception(e)
            print(e)
            node = None
        return json_response(node)

    def delete(self, request):
        """Remove something"""
        try:
            data = json.loads(request.body)
            node = Node.objects.get(id=data.get("id"))
            if node:
                node.delete()
        except Exception as e:
            logging.exception(e)
            print(e)
        return json_response(None)